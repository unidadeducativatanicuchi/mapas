<br>
<h1>DIRECCION DE LA UTC</h1>
<img src="<?php echo base_url('assets/img/marcadorutc.png'); ?>" alt="">
<br>
<div id="mapa1" style="width:100%; height:500px; border:2px solid black;">

</div>
<script type="text/javascript">
  function initMap(){
    //creando una coordenada
    var coordenadaCentral=new google.maps.LatLng(-0.9176191576553565, -78.63303944974578);
    var miMapa=new google.maps.Map(
      document.getElementById('mapa1'),
      {
        center:coordenadaCentral,
        zoom:9,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
    );

    var marcadorUTC= new google.maps.Marker({position: new google.maps.LatLng(-0.9175762478153751, -78.63295361906195),
    map: miMapa,
    title: 'UTC Matriz',
    icon: '<?php echo base_url('assets/img/marcadorutc.png'); ?>'});

    var marcadoSalache= new google.maps.Marker({position: new google.maps.LatLng(-0.999325513719914, -78.62343560916987),
    map: miMapa,
    title: 'UTC Campus Salache'});

    var marcadoPujili= new google.maps.Marker({position: new google.maps.LatLng(-0.9581125523113166, -78.6965450074165),
    map: miMapa,
    title: 'UTC Extension Pujili'});

    var marcadoLaManá= new google.maps.Marker({position: new google.maps.LatLng(-0.9436626933364372, -79.2381002405689),
    map: miMapa,
    title: 'UTC Campus La Maná',
    icon:'<?php echo base_url('assets/img/marcadorutc.png'); ?>'});


  }
</script>
