<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('header');//cargando cabedcera
		$this->load->view('welcome_message');//cargando contenido
		$this->load->view('footer');//cargando pie de pagina
	}
	public function universidad()
	{
		$this->load->view('header');//cargando cabedcera
		$this->load->view('universidad');//cargando contenido
		$this->load->view('footer');//cargando pie de pagina
	}

	public function ciudad()
	{
		$this->load->view('header');//cargando cabedcera
		$this->load->view('ciudad');//cargando contenido
		$this->load->view('footer');//cargando pie de pagina
	}
}//cierre de la clase
